#include <iostream>
using namespace std;

//>abs_not_overloaded
int abs_int(int i) {
    if (i < 0) return -i; else return i;
}
double abs_double(double f) {
    if (f < 0) return -f; else return f;
}
//<abs_not_overloaded

//>abs_overloaded
int abs(int i) {
    if (i < 0) return -i; else return i;
}
double abs(double f) {
    if (f < 0) return -f; else return f;
}
//<abs_overloaded

//>def_par
void print(int i, int talstelsel=10);
//<def_par

//>struct_Tijdsduur
struct Tijdsduur { /* Een Tijdsduur bestaat uit: */
    int uur;       /*     een aantal uren en     */
    int min;       /*     een aantal minuten.    */
};
//<struct_Tijdsduur

int main() {
{
//>som_in_C
    int array[] = {12, 2, 17, 32, 1, 18};
    int i, som = 0, aantal = sizeof array / sizeof array[0];
    for (i = 0; i < aantal; i++) {
        som += array[i];
    }
//<som_in_C
}
{
//>use_abs_overloaded
    double in;
    std::cin >> in; // lees in
    std::cout << abs(in) << std::endl; // druk de absolute waarde van in af
//<use_abs_overloaded
}
{
//>use_def_par
    print(5, 2);  // uitvoer: 101
    print(5);     // uitvoer: 5
    print(5, 10); // uitvoer: 5
//<use_def_par
}
{
//>use_struct_C
    struct Tijdsduur td1;
//<use_struct_C
}
{
//>struct_typedef
typedef struct Tijdsduur TTijdsduur;
//<struct_typedef
//>use_struct_typedef
    TTijdsduur td2;
//<use_struct_typedef
}
{
//>use_struct_Cpp
    Tijdsduur td3;
//<use_struct_Cpp
}
{
    int a, b=1;
    a = ++++b;
    std::cout << "a = " << std::endl;
//  a = b++++;
//  Error (Microsoft): '++' needs l-value
//  Error (GCC): lvalue required as increment operand  
}    
{
//>new
    double* dp(new double); // reserveer een double
    int i; cin >> i;
    double* drij(new double[i]); // reserveer een array met i doubles
    // ...
    delete dp; // geef de door dp aangewezen geheugenruimte vrij
    delete[] drij; // idem voor de door drij aangewezen array
//<new
}



    return 0;
}