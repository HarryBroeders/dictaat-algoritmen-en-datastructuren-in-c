#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
using namespace std;

int telop(int i, int j) {
    return i + j;
}

int main() {
    vector<int> v = {-3, -4, 3, 4};
    vector<int> w = {1, 2, 3, 4};
    ostream_iterator<int> iout(cout, " ");
    copy(v.cbegin(), v.cend(), iout);
    cout << endl;
    copy(w.cbegin(), w.cend(), iout);
    cout << endl;

//  Bewerking opgeven met een functie.
//  Nadeel: voor elke bewerking moet een aparte functie worden geschreven.
    transform(v.cbegin(), v.cend(), w.cbegin(), v.begin(), telop);
    copy(v.cbegin(), v.cend(), iout);
    cout << endl;

//  Bewerking opgeven met een lambda functie.
//  Voordeel: handig als bewerking uniek is.
//  Nadeel: geen hergebruik mogelijk.
    transform(v.cbegin(), v.cend(), w.cbegin(), v.begin(), [](int i1, int i2) {
        return i1 + i2;
    });
    copy(v.cbegin(), v.cend(), iout);
    cout << endl;

    cin.get();
    return 0;
}
