# Dictaat Algoritmen en Datastructuren in C++ #

In dit repository is het dictaat Algoritmen en Datastructuren in C++ opgenomen. 
Je kunt de gecompileerde versies (in pdf) vinden in de [Downloads](https://bitbucket.org/HarryBroeders/dictaat-algoritmen-en-datastructuren-in-c/downloads) van dit repository.

Op- en aanmerkingen zijn altijd welkom. Maak een [issue](https://bitbucket.org/HarryBroeders/dictaat-algoritmen-en-datastructuren-in-c/issues?status=new&status=open) aan of stuur een mail naar [Harry Broeders](mailto:harry@hc11.demon.nl).